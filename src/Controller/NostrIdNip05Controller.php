<?php

namespace Drupal\nostr_id_nip05\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for the well-known path.
 */
class NostrIdNip05Controller extends ControllerBase {

  /**
   * The database connection variable.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The ModuleHandler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   ModuleHandler service.
   */
  public function __construct(Connection $database, ModuleHandlerInterface $moduleHandler) {
    $this->database = $database;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('database'),
          $container->get('module_handler')
      );
  }

  /**
   * Handle request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Information about the current HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function handleRequest(Request $request) {
    $response = new JsonResponse();
    $response->headers->set('Content-Type', 'application/json; charset=utf-8');
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $names_query = $this->database->query('SELECT * FROM {nostr_id_nip05_names}');
    $result = $names_query->fetchAll();
    $data = [
      'names' => [],
      'relays' => [],
    ];
    foreach ($result as $row) {
      $data['names'][$row->handle] = $row->pubkey;
    }
    $response->setData($data);
    $response->setEncodingOptions($response->getEncodingOptions() | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    $this->moduleHandler->alter('nostr_id_nip05', $data);
    return $response;
  }

}
