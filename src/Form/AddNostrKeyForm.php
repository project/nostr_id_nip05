<?php

namespace Drupal\nostr_id_nip05\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use swentel\nostr\Key\Key;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure nostr_id_nip05 settings for this site.
 */
class AddNostrKeyForm extends ConfigFormBase {

  /**
   * Database connection variable.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * HttpClient.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Include the messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Http client.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger service.
   */
  public function __construct(Connection $database, ClientInterface $http_client, MessengerInterface $messenger) {
    $this->database = $database;
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('database'),
      $container->get('http_client'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nostr_id_nip05_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nostr_id_nip05.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Preview current nostr.json.
    $nostr_json_url = Url::fromRoute('nostr_id_nip05.well-known', [], ['absolute' => TRUE])->toString();
    try {
      $nostr_json_request = $this->httpClient->request('get', $nostr_json_url);
    }
    catch (GuzzleException $e) {
      $this->messenger->addError($e->getMessage());
    }
    $nostr_json = sprintf(
          'Current content of <a href="%s">%s/.well-known/nostr.json</a>: <br /> <pre>%s</pre>',
          $nostr_json_url,
          DRUPAL_ROOT,
          $nostr_json_request->getBody()->getContents()
      );
    $form['nostr_json_content'] = [
      '#type' => 'markup',
      '#markup' => $nostr_json,
    ];
    $form['nostr_json_link'] = [
      '#type' => 'markup',
      '#markup' => '<hr />',
    ];
    $form['nostr_names_pubkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your pubkey starting with npub1 or in hex format'),
      '#required' => TRUE,
      '#description' => $this->t('Use <a href="https://damus.io/key/" target="_blank">https://damus.io/key/</a> or <a href="https://github.com/rot13maxi/key-convertr" target="_blank">https://github.com/rot13maxi/key-convertr</a> to convert your bech32 encoded key (starts with npub) to a hex encoded key.'),
    ];
    $form['nostr_names_handle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your handle on Nostr'),
      '#required' => TRUE,
      '#field_prefix' => '@',
    ];
    // @todo add fields for submitting your relays you prefer to post notes.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $pubkey = $form_state->getValue('nostr_names_pubkey');
    if (str_starts_with($pubkey, 'npub')) {
      // @todo validate bech32 encoded key
    }
    else {
      // @todo validate hex encoded key
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $pubkey_value = $form_state->getValue('nostr_names_pubkey');
    if (str_starts_with($pubkey_value, 'npub')) {
      $key = new Key();
      $pubkey = $key->convertToHex($pubkey_value);
    }
    else {
      $pubkey = $pubkey_value;
    }

    $handle = $form_state->getValue('nostr_names_handle');

    $data = [
      'pubkey' => $pubkey,
      'handle' => $handle,
    ];
    // Insert or update names entry in database.
    $this->database->merge('nostr_id_nip05_names')
      ->key('handle', $handle)
      ->fields($data)
      ->execute();
    parent::submitForm($form, $form_state);
  }

}
