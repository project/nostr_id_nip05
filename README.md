# Nostr internet identifier NIP-05

With this module you can setup your Nostr internet identifier.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/nostr_id_nip05).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/nostr_id_nip05).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

- PHP 8.1
- GMP extension - https://www.php.net/manual/en/ref.gmp.php
- XML extension - https://www.php.net/manual/en/ref.xml.php

This module requires no other modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

After you have installed and enabled the module,
the configuration of this module is available on
`/admin/config/system/nostr-id-nip05`.


## Maintainers

- Sebastian Hagens - [Sebastix](https://www.drupal.org/u/sebastian-hagens)
